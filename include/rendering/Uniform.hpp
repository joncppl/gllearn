//
// Created by jonathan on 10/03/19.
//

#ifndef GLLEARN_UNIFORM_HPP
#define GLLEARN_UNIFORM_HPP

#include <pch.hpp>

#include "Program.hpp"

#include <boost/container_hash/hash.hpp>

class Uniform : LogMixin {
public:
    explicit Uniform(std::string &&name);

    explicit Uniform(const std::string &name);

    explicit Uniform(std::string &&name, const Program &program);

    explicit Uniform(const std::string &name, const Program &program);

    explicit Uniform(std::string &&name, unsigned int program_id);

    explicit Uniform(const std::string &name, unsigned int program_id);

    const std::string name() const { return m_name; }

    const int location() const { return m_location; }

    const unsigned int program_id() const { return m_program_id; }

    size_t hash() const;

    bool operator==(const Uniform &other) const;

    void set_program(const Program &program);

    void set_program(unsigned int program_id);

    void set(int);

    void set(unsigned int);

    void set(float);

    void set(float, float, float);

    void set(float, float, float, float);

    void set(const glm::vec3 &);

    void set(const glm::mat4 &);

    void set(const glm::mat3 &);

private:
    int m_location = -1;
    std::string m_name;
    unsigned int m_program_id = 0;

    void get_location();
};

namespace std {
    template<>
    struct hash<Uniform> {
        size_t operator()(Uniform &x) const {
            return x.hash();
        }
    };
}


#endif //GLLEARN_UNIFORM_HPP
