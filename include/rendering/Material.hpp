//
// Created by jonathan on 11/03/19.
//

#ifndef GLLEARN_MATERIAL_HPP
#define GLLEARN_MATERIAL_HPP

#include <pch.hpp>

#include "Program.hpp"
#include "UniformSet.hpp"

class Object;
class Texture;

class Material {
public:
    Material() : m_program(), m_uniforms(m_program) {}

    explicit Material(const Program &program) : m_program(program), m_uniforms(program) {}

    explicit Material(Program &&program) : m_program(std::move(program)), m_uniforms(program) {}

    void set_program(const Program &program) {
        m_program = program;
    }

    Program &program() { return m_program; }

    const bool operator==(const Material &other) const {
        return m_program == other.m_program;
    }

    void use(const std::shared_ptr<Object> &object);

    void set_texture(const std::shared_ptr<Texture> &);

    std::function<void()> get_gui_callback();

    void ambient_strength(float ambient_strength) { m_ambient_strength = ambient_strength; }

private:
    Program m_program;
    UniformSet m_uniforms;
    std::shared_ptr<Texture> m_texture = nullptr;

    glm::vec3 m_material_color{0.4f, 0.0f, 0.4f};
    float m_ambient_strength = 0.5;
    float m_specular_strength = 0.5;
    int m_specular_power = 32;
    glm::vec3 m_light_color{1.0f, 1.0f, 1.0f};

    void render_gui();
};

#endif //GLLEARN_MATERIAL_HPP
