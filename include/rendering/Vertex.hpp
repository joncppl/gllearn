//
// Created by jonathan on 12/03/19.
//

#ifndef GLLEARN_VERTEX_HPP
#define GLLEARN_VERTEX_HPP

#include <pch.hpp>

struct Vertex {
    glm::vec3 coord;
    glm::vec3 normal;
    glm::vec2 uv;

    Vertex(const glm::vec3 &coord_, const glm::vec3 &normal_, const glm::vec2 &uv_) : coord(coord_), normal(normal_),
                                                                                      uv(uv_) {}
};

#endif //GLLEARN_VERTEX_HPP
