//
// Created by jonathan on 09/03/19.
//

#ifndef GLLEARN_SHADER_HPP
#define GLLEARN_SHADER_HPP

#include <pch.hpp>

class Shader : LogMixin {
public:
    Shader(unsigned int type, const std::string &code);

    Shader(unsigned int type, unsigned char *data, unsigned int length);

    ~Shader();

    bool compile();

    unsigned int id() const { return m_id; }

    bool compiled() const { return m_compiled; }

private:
    unsigned int m_id = 0;
    unsigned int m_type;

    std::string m_code;

    bool m_compiled = false;
};

#define MAKE_SHADER(type, fn) Shader((type), (fn), fn ## _len)

#endif //GLLEARN_SHADER_HPP
