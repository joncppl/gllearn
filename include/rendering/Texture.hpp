//
// Created by jonathan on 17/03/19.
//

#ifndef GLLEARN_TEXTURE_HPP
#define GLLEARN_TEXTURE_HPP

#include <pch.hpp>

class Texture : LogMixin {
public:
    explicit Texture(const std::string &filename);

    explicit Texture(std::string &&filename);

    void bind();

    void activate(unsigned int slot = 0);

    unsigned int id() { return m_id; }

private:
    unsigned int m_id = 0;

    std::string m_filename;

    int m_width = 0;
    int m_height = 0;

    void gen_texture();

    void load_buf_textures();
};


#endif //GLLEARN_TEXTURE_HPP
