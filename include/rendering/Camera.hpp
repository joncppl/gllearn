//
// Created by jonathan on 16/03/19.
//

#ifndef GLLEARN_CAMERA_HPP
#define GLLEARN_CAMERA_HPP

#include <pch.hpp>

#include <rendering/UniformSet.hpp>
#include <rendering/Program.hpp>

class Camera {
public:
    glm::mat4 view_matrix() const;

    glm::mat4 projection_matrix() const;

    glm::vec3 &position() { return m_position; }

    glm::vec3 &target() { return m_target; }

    glm::vec3 &up() { return m_up; }

    void translate_position(const glm::vec3 &);

    void translation_target(const glm::vec3 &);

    void translate_up(const glm::vec3 &);

    float fov() const { return m_fov; }

    float display_w() const { return m_display_w; }

    float display_h() const { return m_display_h; }

    float near_clip_plane() const { return m_near_clip_plane; }

    float far_clip_plane() const { return m_far_clip_plane; }

    void fov(float fov);

    void display_w(float display_w);

    void display_h(float display_h);

    void near_clip_plane(float near_clip_plane);

    void far_clip_plane(float far_clip_plane);

    void set_uniforms(const Program &);

    void set_uniforms(unsigned int program_id);

    void set_uniforms(const std::shared_ptr<Program> &);

    std::function<void()> get_gui_callback();

private:
    glm::vec3 m_position = {0.0f, 0.0f, -2.0f};
    glm::vec3 m_target = {0.0f, 0.0f, 0.0f};
    glm::vec3 m_up = {0.0f, 1.0f, 0.0f};

    float m_fov = 49.1f;
    float m_display_w;
    float m_display_h;
    float m_near_clip_plane = 0.1f;
    float m_far_clip_plane = 100.0f;

    UniformSet m_uniform_set;

    void do_set_uniforms(unsigned int program_id);

    void render_gui();
};


#endif //GLLEARN_CAMERA_HPP
