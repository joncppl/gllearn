//
// Created by jonathan on 11/03/19.
//

#ifndef GLLEARN_RENDERER_HPP
#define GLLEARN_RENDERER_HPP

#include <pch.hpp>

#include "Object.hpp"
#include "BaseBuffer.hpp"
#include "VertexArray.hpp"
#include "Vertex.hpp"
#include "Camera.hpp"

#include <boost/container_hash/hash.hpp>

class Renderer : LogMixin {
public:
    explicit Renderer(VertexArrayLayout &layout) : m_vertex_array_layout(std::move(layout)) {}

    void add_object(const std::shared_ptr<Object> &object);

    void render(const std::shared_ptr<Camera> &camera);

private:
    class RenderEntry {
    public:
        std::shared_ptr<ArrayBuffer<Vertex>> array_buffer = std::make_shared<ArrayBuffer<Vertex>>();
        std::shared_ptr<IndexBuffer<unsigned int>> index_buffer = std::make_shared<IndexBuffer<unsigned int>>();
        std::shared_ptr<VertexArray> vao;
        std::shared_ptr<Object> object;

        RenderEntry(const VertexArrayLayout &layout, const std::shared_ptr<Object> &object_) : vao(
                std::make_shared<VertexArray>(layout)), object(object_) {}
    };

    std::vector<RenderEntry> m_entries;

    VertexArrayLayout m_vertex_array_layout;

    void draw_call(unsigned int count);
};

#endif //GLLEARN_RENDERER_HPP
