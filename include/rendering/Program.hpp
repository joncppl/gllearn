//
// Created by jonathan on 09/03/19.
//

#ifndef GLLEARN_PROGRAM_HPP
#define GLLEARN_PROGRAM_HPP

#include <pch.hpp>

#include "Shader.hpp"

class Program : LogMixin {
public:
    explicit Program(std::size_t l);

    Program();

    explicit Program(std::vector<Shader> &&shaders);

    ~Program();

    void add_shader(const Shader &&shader) {
        m_shaders.emplace_back(std::move(shader));
    }

    unsigned int id() const { return m_id; }

    void use();

    bool link();

    const bool operator==(const Program &other) const {
        return m_id == other.m_id;
    }

private:
    unsigned int m_id = 0;

    std::vector<Shader> m_shaders;

    void create_program();
};


#endif //GLLEARN_PROGRAM_HPP
