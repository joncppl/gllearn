//
// Created by jonathan on 10/03/19.
//

#include <pch.hpp>

#ifndef GLLEARN_VERTEXARRAY_HPP
#define GLLEARN_VERTEXARRAY_HPP

struct VertexArrayLayoutElement {
    int count;
    unsigned int type;
    unsigned char normalized;
};

namespace ver_resolver {
    template<typename T>
    inline unsigned int resolve() {
        return 0;
    }

    template<>
    inline unsigned int resolve<float>() {
        return GL_FLOAT;
    }

    template<>
    inline unsigned int resolve<unsigned int>() {
        return GL_UNSIGNED_INT;
    }

    template<>
    inline unsigned int resolve<unsigned char>() {
        return GL_UNSIGNED_BYTE;
    }
}

class VertexArrayLayout {
public:
    explicit VertexArrayLayout(const std::vector<VertexArrayLayoutElement> &elements) : m_elements(elements) {}

    explicit VertexArrayLayout(std::vector<VertexArrayLayoutElement> &&elements) : m_elements(std::move(elements)) {}

    explicit VertexArrayLayout() {}

    void add_element_direct(const VertexArrayLayoutElement &element) { m_elements.emplace_back(element); }

    template<typename T>
    void add_element(int count) {
        auto type = ver_resolver::resolve<T>();
        add_element_direct({count, type, GL_FALSE});
        m_stride += count * element_size(type);
    }

    const std::vector<VertexArrayLayoutElement> &elements() { return m_elements; }

    static unsigned int element_size(unsigned int type);

    unsigned int stride() const { return m_stride; }

private:
    std::vector<VertexArrayLayoutElement> m_elements;
    unsigned int m_stride = 0;
};

class VertexArray : LogMixin {
public:
    VertexArray() {
        gen_arrays();
    }

    explicit VertexArray(const VertexArrayLayout &layout) : m_layout(layout) {
        gen_arrays();
    }

    explicit VertexArray(VertexArrayLayout &&layout) : m_layout(std::move(layout)) {
        gen_arrays();
    }

    ~VertexArray() {
        if (m_id) {
            glDeleteVertexArrays(1, &m_id);
        }
    }

    void set_layout(const VertexArrayLayout &layout) {
        m_layout = layout;
    }

    VertexArrayLayout &layout() {
        return m_layout;
    }

    void bind();

    void set_attributes();

private:
    unsigned int m_id = 0;
    VertexArrayLayout m_layout;

    void gen_arrays();
};

#endif //GLLEARN_VERTEXARRAY_HPP
