//
// Created by jonathan on 11/03/19.
//

#ifndef GLLEARN_OBJECT_HPP
#define GLLEARN_OBJECT_HPP

#include <pch.hpp>

#include "Material.hpp"
#include "Vertex.hpp"

class EventManager;

class Object : LogMixin {
public:
    Object(const std::string &name, std::shared_ptr<Material> material = nullptr);

    Object(const std::string &name, const glm::vec3 &translation, const glm::quat &rotation, glm::vec3 &scale,
           std::shared_ptr<Material> material = nullptr);

    Object(const std::string &name, glm::vec3 &&translation, glm::quat &&rotation, glm::vec4 &&scale,
           std::shared_ptr<Material> material = nullptr);

    glm::mat4 translation_matrix() const;

    glm::mat4 rotation_matrix() const;

    glm::mat4 scale_matrix() const;

    glm::mat4 model_matrix() const;

    glm::vec3 &translation() { return m_translation; }

    glm::quat &rotation_quat() { return m_rotation; }

    glm::vec3 rotation_euler() const { return glm::eulerAngles(m_rotation); }

    glm::vec3 &scale() { return m_scale; }

    void translate(const glm::vec3 &translate);

    void rotate(const glm::vec3 &euler);

    void rotate(const glm::quat &quat);

    void scale(const glm::vec3 &scale);

    void set_translation(const glm::vec3 &translation);

    void set_rotation(const glm::quat &quat);

    void set_rotation(const glm::vec3 &euler);

    void set_scale(const glm::vec3 &scale);

    void set_material(const std::shared_ptr<Material> &material);

    std::vector<Vertex> &vertices();

    std::vector<unsigned int> &indicies();

    std::shared_ptr<Material> &material();

    const std::string &name() const { return m_name; }

    const std::vector<Vertex> &get_vertices() { return m_vertices; }

    const std::vector<unsigned int> &get_indicies() { return m_indicies; }

    const std::shared_ptr<Material> &get_material() { return m_material; }

    bool changed() const;

    void clear_changed();

    std::function<void()> get_gui_callback();

    void register_event_callbacks(std::shared_ptr<EventManager>);

    void update(float delta_seconds);

private:
    std::vector<Vertex> m_vertices;
    std::string m_name;

    bool m_changed = true;

    std::vector<unsigned int> m_indicies;

    glm::vec3 m_translation = {0.0f, 0.0f, 0.0f};
    glm::quat m_rotation = {0.0f, 0.0f, 0.0f, 0.0f};
    glm::vec3 m_scale = {1.0f, 1.0f, 1.0f};

    float m_speed = 1.0f;
    glm::vec3 m_velocity = {0.0f, 0.0f, 0.0f};

    std::shared_ptr<Material> m_material = nullptr;


    glm::vec3 m_gui_rot;

    void render_gui();

    void key_event_callback(int, int);
};


#endif //GLLEARN_OBJECT_HPP
