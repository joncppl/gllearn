//
// Created by jonathan on 16/03/19.
//

#ifndef GLLEARN_UNIFORMSET_HPP
#define GLLEARN_UNIFORMSET_HPP

#include <pch.hpp>

#include "Uniform.hpp"

class UniformSet : LogMixin {
public:
    UniformSet();

    explicit UniformSet(unsigned int program_id);

    explicit UniformSet(const Program &program);

    std::shared_ptr<Uniform> uniform(std::string &name);

    std::shared_ptr<Uniform> uniform(std::string &&name);

    void set_program(const Program &program);

    void set_program(unsigned int program_id);

private:
    unsigned int m_program_id = 0;
    std::unordered_map<std::string, std::shared_ptr<Uniform>> m_uniforms;
};


#endif //GLLEARN_UNIFORMSET_HPP
