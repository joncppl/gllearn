#ifndef PCH_HPP
#define PCH_HPP

#include <vector>
#include <thread>
#include <string>
#include <memory>
#include <unordered_map>
#include <unordered_set>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>

#include <imgui.h>
#include <imgui_impl_glfw.h>
#include <imgui_impl_opengl3.h>

#include <utility/log.hpp>

#endif // PCH_HPP