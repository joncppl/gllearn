//
// Created by jonathan on 17/03/19.
//

#ifndef GLLEARN_EVENTMANAGER_HPP
#define GLLEARN_EVENTMANAGER_HPP

#include <pch.hpp>

class EventManager : LogMixin {
public:
    class ActionFilter {
    public:
        ActionFilter() {}

        ActionFilter(const std::unordered_set<int> &codes_, const std::unordered_set<int> &actions_) : codes(codes_),
                                                                                                       actions(actions_) {}

        ActionFilter(std::unordered_set<int> &&codes_, std::unordered_set<int> &&actions_) : codes(std::move(codes_)),
                                                                                             actions(std::move(
                                                                                                     actions_)) {}
        bool operator==(const ActionFilter &other) const {
            return codes == other.codes && actions == other.actions;
        }

        std::unordered_set<int> codes;
        std::unordered_set<int> actions;
    };

    using KeyEventCallback = std::function<void(int, int)>;
    using CursorPositionEventCallback = std::function<void(double, double)>;
    using CursorEnterEventCallback = std::function<void(bool)>;
    using MouseButtonEventCallback = std::function<void(int, int, double, double)>;
    using ScrollEventCallback = std::function<void(double, double)>;

    void add_key_event_callback(KeyEventCallback, ActionFilter= ActionFilter());

    void add_cursor_position_event_callback(CursorPositionEventCallback);

    void add_cursor_enter_event_callback(CursorEnterEventCallback);

    void add_mouse_button_event_callback(MouseButtonEventCallback, ActionFilter= ActionFilter());

    void add_scroll_event_callback(ScrollEventCallback);

    void remove_key_event_callback(KeyEventCallback, ActionFilter= ActionFilter());

    void remove_cursor_position_event_callback(CursorPositionEventCallback);

    void remove_cursor_enter_event_callback(CursorEnterEventCallback);

    void remove_mouse_button_event_callback(MouseButtonEventCallback, ActionFilter= ActionFilter());

    void remove_scroll_event_callback(ScrollEventCallback);

    void trigger_key_event(int key, int scancode, int action, int mods);

    void trigger_cursor_position_event(double xpos, double ypos);

    void trigger_cursor_enter_event(int entered);

    void trigger_mouse_button_event(int button, int action, int mods);

    void trigger_scroll_event(double xoffset, double yoffset);

    static const char *key_to_string(int key);

    static const char *action_to_string(int action);

    static const char *mouse_button_to_string(int button);

private:
    double m_x_mouse_pos = 0.0;
    double m_y_mouse_pos = 0.0;

    std::vector<std::pair<ActionFilter, KeyEventCallback>> m_key_event_callbacks;

    std::vector<CursorPositionEventCallback> m_cursor_position_event_callbacks;

    std::vector<CursorEnterEventCallback> m_cursor_enter_event_callbacks;

    std::vector<std::pair<ActionFilter, MouseButtonEventCallback>> m_mouse_button_event_callbacks;

    std::vector<ScrollEventCallback> m_scroll_event_callbacks;
};


#endif //GLLEARN_EVENTMANAGER_HPP
