//
// Created by jonathan on 09/03/19.
//

#ifndef GLLEARN_LOG_HPP
#define GLLEARN_LOG_HPP

#include <spdlog/spdlog.h>

void initialize_logger();

std::shared_ptr<spdlog::logger> get_logger();

class LogMixin {
protected:
    std::shared_ptr<spdlog::logger> log = get_logger();

    template<typename... Args>
    void trace(const char *fmt, const Args &... args) {
        log->trace(fmt, args ...);
    }

    template<typename... Args>
    void debug(const char *fmt, const Args &... args) {
        log->debug(fmt, args ...);
    }

    template<typename... Args>
    void info(const char *fmt, const Args &... args) {
        log->info(fmt, args ...);
    }

    template<typename... Args>
    void warn(const char *fmt, const Args &... args) {
        log->warn(fmt, args ...);
    }

    template<typename... Args>
    void error(const char *fmt, const Args &... args) {
        log->error(fmt, args ...);
    }

    template<typename... Args>
    void critical(const char *fmt, const Args &... args) {
        log->critical(fmt, args ...);
    }

    void set_level(spdlog::level::level_enum);
};

#endif //GLLEARN_LOG_HPP
