//
// Created by jonathan on 12/03/19.
//

#ifndef GLLEARN_OBJLOADER_HPP
#define GLLEARN_OBJLOADER_HPP

#include <pch.hpp>

#include <regex>
#include <fstream>

#include "rendering/Object.hpp"

class MeshLoader {
public:
    static bool load(std::shared_ptr<Object> &object, const std::string &filename);
};

#endif //GLLEARN_OBJLOADER_HPP
