//
// Created by jonathan on 13/03/19.
//

#ifndef GLLEARN_OVERLAYGUI_HPP
#define GLLEARN_OVERLAYGUI_HPP

#include <pch.hpp>

class Object;
class Camera;
class MainWindow;
class Material;

class OverlayGui : public LogMixin {
public:
    OverlayGui(const MainWindow &window);

    void add_callback(const std::function<void()> &cb);

    void add_callback(std::function<void()> &&cb);

    void add_object(Object &object);

    void add_object(const std::shared_ptr<Object> &);

    void add_camera(Camera &);

    void add_camera(const std::shared_ptr<Camera> &);

    void add_material(Material &material);

    void add_material(const std::shared_ptr<Material> &material);

    void render();

private:
    const char *imgui_glsl_version = "#version 150";

    std::vector<std::function<void()>> m_render_callbacks;

};

#endif //GLLEARN_OVERLAYGUI_HPP
