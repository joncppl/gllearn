//
// Created by jonathan on 09/03/19.
//

#ifndef GLLEARN_MAINWINDOW_HPP
#define GLLEARN_MAINWINDOW_HPP

#include <pch.hpp>

struct GLFWwindow;
class EventManager;

class MainWindow : LogMixin {
public:
    MainWindow(const std::string &title, int width, int height, std::shared_ptr<EventManager>);

    bool window_should_close();

    void swap_buffers();

    bool success() const { return m_window != nullptr; }

    ~MainWindow();

    GLFWwindow *window() const { return m_window; }

    void key_callback(int key, int scancode, int action, int mods);

    void cursor_position_callback(double xpos, double ypos);

    void cursor_enter_callback(int entered);

    void mouse_button_callback(int button, int action, int mods);

    void scroll_callback(double xoffset, double yoffset);


private:
    std::string m_title;
    int m_width;
    int m_height;

    GLFWwindow *m_window = nullptr;

    std::shared_ptr<EventManager> m_event_manager;

    void destroy();

    bool configure_input_callbacks();

    static void key_callback_dispatch(GLFWwindow* window, int key, int scancode, int action, int mods);

    static void cursor_position_callback_dispatch(GLFWwindow* window, double xpos, double ypos);

    static void cursor_enter_callback_dispatch(GLFWwindow* window, int entered);

    static void mouse_button_callback_dispatch(GLFWwindow* window, int button, int action, int mods);

    static void scroll_callback_dispatch(GLFWwindow* window, double xoffset, double yoffset);
};


#endif //GLLEARN_MAINWINDOW_HPP
