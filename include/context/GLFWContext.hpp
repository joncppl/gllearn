//
// Created by jonathan on 09/03/19.
//

#ifndef GLLEARN_GLFWCONTEXT_HPP
#define GLLEARN_GLFWCONTEXT_HPP

#include <pch.hpp>

class GLFWContext : LogMixin {
public:
    GLFWContext();

    ~GLFWContext();

    bool initialized() const { return m_initialized; }

    void poll_events();

private:
    bool m_initialized = false;

    static void error_callback(int errco, const char *errmsg);
};


#endif //GLLEARN_GLFWCONTEXT_HPP
