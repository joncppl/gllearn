//
// Created by jonathan on 09/03/19.
//

#ifndef GLLEARN_GLCONTEXT_HPP
#define GLLEARN_GLCONTEXT_HPP

#include <pch.hpp>

class GLContext : LogMixin {
public:
    GLContext();

    ~GLContext();

    std::string get_gl_version();

    bool initialized() const { return m_initialized; }

private:
    bool m_initialized = false;
};


#endif //GLLEARN_GLCONTEXT_HPP
