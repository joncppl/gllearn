//
// Created by jonathan on 09/03/19.
//

#ifndef GLLEARN_BASEBUFFER_HPP
#define GLLEARN_BASEBUFFER_HPP

#include <pch.hpp>

template<typename T, template <typename> class C>
class BaseBuffer : LogMixin {
public:
    BaseBuffer() {
        gen_buffer();
    }

    explicit BaseBuffer(unsigned int l) {
        gen_buffer();
        reserve(l);
    }

    explicit BaseBuffer(std::vector<T> &&data) : m_data(std::move(data)) {
        gen_buffer();
    }

    ~BaseBuffer() {
        if (m_id > 0) {
            glDeleteBuffers(1, &m_id);
        }
    }

    template<typename ...IL>
    void add(IL const && ... il) {
        m_data.emplace_back(il ...);
    }

    void extend(const std::vector<T> &data) {
        debug("extending buffer {} entries", data.size());
        size_t new_size = m_data.size() + data.size();
        if (fit_uint(new_size)) {
            reserve(static_cast<unsigned int>(new_size)); // if enough is already reserved, this does nothing
            m_data.insert(m_data.end(), data.begin(), data.end());
        }
    }

    void reserve(unsigned int l) {
        m_data.reserve(l);
    }

    unsigned int count() {
        auto s = m_data.size();
        if (fit_uint(s)) {
            return static_cast<unsigned int>(s);
        }
        return 0;
    }

    unsigned int size() {
        return count() * elem_size();
    }

    unsigned int elem_size() {
        return sizeof(T);
    }

    void clear() {
        m_data.clear();
    }

    void bind() {
        log->debug("bind buffer id={}.", m_id);
        glBindBuffer(C<T>::buffer_type(), m_id);
    }

    void *ptr() {
        return m_data.data();
    }

    void buffer() {
        bind();
        log->debug("buffer data id={} size={} ptr={}", m_id, size(), ptr());
        glBufferData(C<T>::buffer_type(), size(), ptr(), GL_STATIC_DRAW);
    }

    static unsigned int buffer_type() {
        return 0;
    }

    bool overflowed() { return m_overflow; }

    unsigned int id() { return m_id; }

private:
    unsigned int m_id = 0;
    std::vector<T> m_data;

    bool m_overflow = false;

    bool fit_uint(size_t l) {
        if (l > std::numeric_limits<unsigned int>::max()) {
            m_overflow = true;
            return false;
        }
        return true;
    }

    void gen_buffer() {
        glGenBuffers(1, &m_id);
        if (!m_id) {
            log->error("glGenBuffers failed.");
        }
        log->debug("Constructed buffer id={}.", m_id);
    }
};

template<typename T>
class ArrayBuffer : public BaseBuffer<T, ArrayBuffer> {
public:
    using BaseBuffer<T, ArrayBuffer>::BaseBuffer;

    static unsigned int buffer_type() {
        return GL_ARRAY_BUFFER;
    }
};

template<typename T>
class IndexBuffer : public BaseBuffer<T, IndexBuffer> {
public:
    using BaseBuffer<T, IndexBuffer>::BaseBuffer;

    static unsigned int buffer_type() {
        return GL_ELEMENT_ARRAY_BUFFER;
    }
};



#endif //GLLEARN_BASEBUFFER_HPP
