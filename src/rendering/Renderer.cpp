//
// Created by jonathan on 13/03/19.
//

#include <rendering/Renderer.hpp>

#include "rendering/Renderer.hpp"


void Renderer::add_object(const std::shared_ptr<Object> &object) {
    assert(object->material());

    RenderEntry entry(m_vertex_array_layout, object);
    entry.array_buffer->extend(object->get_vertices());
    entry.index_buffer->extend(object->get_indicies());
    entry.array_buffer->buffer();
    entry.index_buffer->buffer();
    entry.vao->set_attributes();

    m_entries.emplace_back(entry);
}

void Renderer::render(const std::shared_ptr<Camera> &camera) {
    trace("START RENDER");

    for (auto &entry : m_entries) {
        entry.object->get_material()->use(entry.object);
        camera->set_uniforms(entry.object->get_material()->program());

        entry.vao->bind();
        entry.array_buffer->bind();
        entry.index_buffer->bind();
        draw_call(entry.index_buffer->count());
    }
}

void Renderer::draw_call(unsigned int count) {
    glDrawElements(GL_TRIANGLES, count, GL_UNSIGNED_INT, nullptr);
}
