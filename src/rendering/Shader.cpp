//
// Created by jonathan on 09/03/19.
//

#include "rendering/Shader.hpp"

Shader::Shader(unsigned int type, const std::string &code) : m_type(type), m_code(code) {
}


Shader::Shader(unsigned int type, unsigned char *data, unsigned int length) : m_type(type) {
    m_code.assign(data, data + length);
}

Shader::~Shader() {
    if (m_id > 0) {
        trace("Deleting shader {}.", m_id);
        glDeleteShader(m_id);
    }
}

bool Shader::compile() {
    m_id = glCreateShader(m_type);

    trace("Compiling shader {}", m_id);

    if (m_id == 0) {
        error("Shader creation failed.");
        return false;
    }

    auto code_ptr = m_code.c_str();

    glShaderSource(m_id, 1, &code_ptr, nullptr);

    glCompileShader(m_id);

    int result;

    glGetShaderiv(m_id, GL_COMPILE_STATUS, &result);

    if (result == GL_FALSE) {
        int len;
        glGetShaderiv(m_id, GL_INFO_LOG_LENGTH, &len);

        auto message = (char *) alloca(len);
        glGetShaderInfoLog(m_id, len, &len, message);

        error("Failed to compile shader {}: {}", m_id, message);
        error("{}", m_code);

        glDeleteShader(m_id);
        m_id = -1;

        return false;
    }

    m_compiled = true;
    return true;
}
