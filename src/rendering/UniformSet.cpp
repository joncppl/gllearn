//
// Created by jonathan on 16/03/19.
//

#include <rendering/UniformSet.hpp>

#include "rendering/UniformSet.hpp"

UniformSet::UniformSet() : m_program_id(0) {
    trace("Default construct UniformSet pid={}", 0);
}

UniformSet::UniformSet(unsigned int program_id) : m_program_id(program_id) {
    trace("Construct UniformSet pid={}", program_id);
}

UniformSet::UniformSet(const Program &program) : m_program_id(program.id()) {
    trace("Construct UniformSet pid={}", program.id());
}

std::shared_ptr<Uniform> UniformSet::uniform(std::string &name) {
    auto found = m_uniforms.find(name);
    if (found == m_uniforms.end()) {
        auto uniform = std::make_shared<Uniform>(name, m_program_id);
        m_uniforms.insert_or_assign(name, uniform);
        return uniform;
    }

    return found->second;
}

std::shared_ptr<Uniform> UniformSet::uniform(std::string &&name) {
    auto found = m_uniforms.find(name);
    if (found == m_uniforms.end()) {
        auto uniform = std::make_shared<Uniform>(name, m_program_id);
        m_uniforms.insert_or_assign(name, uniform);
        return uniform;
    }

    return found->second;
}

void UniformSet::set_program(const Program &program) {
    for (auto &pair : m_uniforms) {
        pair.second->set_program(program);
    }
}

void UniformSet::set_program(unsigned int program_id) {
    for (auto &pair : m_uniforms) {
        pair.second->set_program(program_id);
    }
}