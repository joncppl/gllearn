//
// Created by jonathan on 16/03/19.
//

#include <rendering/Camera.hpp>

#include "rendering/Camera.hpp"

glm::mat4 Camera::view_matrix() const {
    return glm::lookAt(m_position, m_target, m_up);
}

glm::mat4 Camera::projection_matrix() const {
    return glm::perspective(glm::radians(m_fov), m_display_w / m_display_h, m_near_clip_plane, m_far_clip_plane);
}

void Camera::translate_position(const glm::vec3 &translate) {
    m_position += translate;
}

void Camera::translation_target(const glm::vec3 &translate) {
    m_target += translate;
}

void Camera::translate_up(const glm::vec3 &translate) {
    m_up += translate;
}

void Camera::fov(float fov) {
    m_fov = fov;
}

void Camera::display_w(float display_w) {
    m_display_w = display_w;
}

void Camera::display_h(float display_h) {
    m_display_h = display_h;
}

void Camera::near_clip_plane(float near_clip_plane) {
    m_near_clip_plane = near_clip_plane;
}

void Camera::far_clip_plane(float far_clip_plane) {
    m_far_clip_plane = far_clip_plane;
}

void Camera::set_uniforms(const Program &program) {
    do_set_uniforms(program.id());
}

void Camera::set_uniforms(unsigned int program_id) {
    do_set_uniforms(program_id);
}

void Camera::set_uniforms(const std::shared_ptr<Program> &program) {
    do_set_uniforms(program->id());
}

void Camera::do_set_uniforms(unsigned int program_id) {
    m_uniform_set.set_program(program_id);
    m_uniform_set.uniform("u_view_matrix")->set(view_matrix());
    m_uniform_set.uniform("u_projection_matrix")->set(projection_matrix());
    m_uniform_set.uniform("u_camera_pos")->set(m_position);
}

std::function<void()> Camera::get_gui_callback() {
    return std::bind(&Camera::render_gui, this);
}

void Camera::render_gui() {
    ImGui::Begin("Camera");

    ImGui::SliderFloat3("position", &m_position.x, -10.0f, 10.0f);
    ImGui::SliderFloat3("target", &m_target.x, -10.0f, 10.0f);
    ImGui::SliderFloat3("up", &m_up.x, -10.0f, 10.0f);
    ImGui::SliderFloat("fov", &m_fov, 0.01f, 173.0f);
    ImGui::SliderFloat("near clip plane", &m_near_clip_plane, 0.1f, 50.0f);
    ImGui::SliderFloat("far clip plane", &m_far_clip_plane, 0.1f, 50.0f);

    ImGui::End();
}
