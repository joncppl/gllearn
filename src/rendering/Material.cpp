//
// Created by jonathan on 16/03/19.
//

#include <rendering/Material.hpp>

#include <rendering/Material.hpp>
#include <rendering/Object.hpp>
#include <rendering/Texture.hpp>

void Material::use(const std::shared_ptr<Object> &object) {
    m_program.use();

    auto model_matrix = object->model_matrix();
    auto inverse_model_matrix = glm::mat3(glm::transpose(glm::inverse(model_matrix)));

    m_uniforms.uniform("u_model_matrix")->set(model_matrix);
    m_uniforms.uniform("u_transpose_inverse_model_matrix")->set(inverse_model_matrix);

    m_uniforms.uniform("u_material_color")->set(m_material_color);
    m_uniforms.uniform("u_ambient_strength")->set(m_ambient_strength);
    m_uniforms.uniform("u_specular_strength")->set(m_specular_strength);
    m_uniforms.uniform("u_specular_power")->set(m_specular_power);
    m_uniforms.uniform("u_light_color")->set(m_light_color);

    if (m_texture) {
        m_texture->activate(0);
        m_uniforms.uniform("texture1")->set((int)0);
    }
}

std::function<void()> Material::get_gui_callback() {
    return std::bind(&Material::render_gui, this);
}

void Material::render_gui() {
    ImGui::Begin("Main Material");

    ImGui::SliderFloat3("material_color", &m_material_color.x, 0.0f, 1.0f);
    ImGui::SliderFloat("ambient_strength", &m_ambient_strength, 0.0f, 1.0f);
    ImGui::SliderFloat("specular_strength", &m_specular_strength, 0.0f, 1.0f);
    ImGui::SliderInt("specular_power", &m_specular_power, 0, 1024);
    ImGui::SliderFloat3("light color", &m_light_color.x, 0.0f, 1.0f);

    ImGui::End();
}

void Material::set_texture(const std::shared_ptr<Texture> &texture) {
    m_texture = texture;
}
