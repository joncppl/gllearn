//
// Created by jonathan on 10/03/19.
//

#include <rendering/Uniform.hpp>

#include "rendering/Uniform.hpp"

Uniform::Uniform(std::string &&name) : m_name(std::move(name)) {
    trace("construct uniform");
}

Uniform::Uniform(std::string &&name, const Program &program) : m_name(std::move(name)), m_program_id(program.id()) {
    trace("construct uniform");
    get_location();
}

Uniform::Uniform(std::string &&name, unsigned int program_id) : m_name(std::move(name)), m_program_id(program_id) {
    trace("construct uniform");
    get_location();
}

Uniform::Uniform(const std::string &name) : m_name(name) {
    trace("construct uniform");
}

Uniform::Uniform(const std::string &name, const Program &program) : m_name(name), m_program_id(program.id()) {
    trace("construct uniform");
    get_location();
}

Uniform::Uniform(const std::string &name, unsigned int program_id) : m_name(name), m_program_id(program_id) {
    trace("construct uniform");
    get_location();

}

void Uniform::set(int i1) {
//    trace("set {} (pid={}) = ({})", m_name, m_program_id, i1);
    if (m_location > -1) {
        glUniform1i(m_location, i1);
    }
}

void Uniform::set(unsigned int ui1) {
//    trace("set {} (pid={}) = ({})", m_name, m_program_id, ui1);
    if (m_location > -1) {
        glUniform1ui(m_location, ui1);
    }
}

void Uniform::set(float u1) {
//    trace("set {} (pid={}) = ({})", m_name, m_program_id, u1);
    if (m_location > -1) {
        glUniform1f(m_location, u1);
    }

}

void Uniform::set(float u1, float u2, float u3) {
//    trace("set {} (pid={}) = ({}, {}, {})", m_name, m_program_id, u1, u2, u3);
    if (m_location > -1) {
        glUniform3f(m_location, u1, u2, u3);
    }
}

void Uniform::set(float u1, float u2, float u3, float u4) {
//    trace("set {} (pid={}) = ({}, {}, {}, {})", m_name, m_program_id, u1, u2, u3, u4);
    if (m_location > -1) {
        glUniform4f(m_location, u1, u2, u3, u4);
    }
}

void Uniform::set(const glm::vec3 &vec) {
//    trace("set {} (pid={}) = ({}, {}, {})", m_name, m_program_id, vec.x, vec.y, vec.z);
    if (m_location > -1) {
        glUniform3fv(m_location, 1, &vec.x);
    }
}

void Uniform::set(const glm::mat3 &mat) {
//    trace("set {} (pid={}) = [[{}, {}, {}], [{}, {}, {}], [{}, {}, {}]]", m_name, m_program_id, mat[0][0],
//               mat[0][1], mat[0][2], mat[1][0], mat[1][1], mat[1][2], mat[2][0], mat[2][1], mat[2][2]);
    if (m_location > -1) {
        glUniformMatrix3fv(m_location, 1, GL_FALSE, &mat[0][0]);
    }
}

void Uniform::set(const glm::mat4 &mat) {
//    trace("set {} (pid={}) = [[{}, {}, {}, {}], [{}, {}, {}, {}], [{}, {}, {}, {}], [{}, {}, {}, {}]]", m_name,
//               m_program_id, mat[0][0], mat[0][1], mat[0][2], mat[0][3], mat[1][0], mat[1][1], mat[1][2], mat[1][3],
//               mat[2][0], mat[2][1], mat[2][2], mat[2][3], mat[3][0], mat[3][1], mat[3][2], mat[3][3]);
    if (m_location > -1) {
        glUniformMatrix4fv(m_location, 1, GL_FALSE, &mat[0][0]);
    }
}

void Uniform::get_location() {
    m_location = glGetUniformLocation(m_program_id, m_name.c_str());
    if (m_location == -1) {
        warn("glGetUniformLocation for uniform {} failed.", m_name);
    }
}

void Uniform::set_program(const Program &program) {
    m_program_id = program.id();
    get_location();
}

void Uniform::set_program(unsigned int program_id) {
    m_program_id = program_id;
    get_location();
}

size_t Uniform::hash() const {
    size_t seed = 0;
    boost::hash_combine(seed, m_name);
    boost::hash_combine(seed, m_location);
    boost::hash_combine(seed, m_program_id);
    return seed;
}

bool Uniform::operator==(const Uniform &other) const {
    return m_name == other.m_name && m_location == other.m_location && m_program_id == other.m_program_id;
}
