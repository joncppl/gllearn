//
// Created by jonathan on 13/03/19.
//

#include <rendering/VertexArray.hpp>

unsigned int VertexArrayLayout::element_size(unsigned int type) {
    switch (type) {
        case GL_FLOAT:
            return sizeof(float);
        case GL_UNSIGNED_INT:
            return sizeof(unsigned int);
        case GL_UNSIGNED_BYTE:
            return sizeof(unsigned char);
        default:
            return 0;
    }
}

void VertexArray::bind() {
    trace("bind va id={}.", m_id);
    glBindVertexArray(m_id);
}

void VertexArray::set_attributes() {
    bind();
    trace("set attributes va id={}.", m_id);

    unsigned int index = 0;
    unsigned int offset = 0;
    for (const auto &element : m_layout.elements()) {
        glEnableVertexAttribArray(index);

        trace("set attribute index={} count={} type={} normalize={} stride={} offset={}", index, element.count,
                   element.type, element.normalized, m_layout.stride(), offset);
        glVertexAttribPointer(index, element.count, element.type, element.normalized, m_layout.stride(),
                              reinterpret_cast<const void *>(offset));

        offset += element.count * VertexArrayLayout::element_size(element.type);
        index++;
    }
}

void VertexArray::gen_arrays() {
    glGenVertexArrays(1, &m_id);
    if (m_id == 0) {
        trace("glGenVertexArrays failed.");
    }
    trace("generated va id={}.", m_id);
}
