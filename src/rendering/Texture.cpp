//
// Created by jonathan on 17/03/19.
//

#include <rendering/Texture.hpp>

#include "rendering/Texture.hpp"

#include <stb_image.h>

Texture::Texture(const std::string &filename) : m_filename(filename) {
    gen_texture();
    load_buf_textures();
}

Texture::Texture(std::string &&filename) : m_filename(std::move(filename)) {
    gen_texture();
    load_buf_textures();
}

void Texture::bind() {
    trace("bind texture {}", m_id);
    glBindTexture(GL_TEXTURE_2D, m_id);
}

void Texture::gen_texture() {
    glGenTextures(1, &m_id);
    trace("generated texture {}", m_id);
    bind();
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glEnable(GL_TEXTURE_2D);
}

void Texture::load_buf_textures() {
    int channels;
    trace("Loading {}.", m_filename);
    stbi_set_flip_vertically_on_load(true);
    unsigned char *data = stbi_load(m_filename.c_str(), &m_width, &m_height, &channels, 0);
    if (data != nullptr) {
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, m_width, m_height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
        glGenerateMipmap(GL_TEXTURE_2D);
    } else {
        error("Failed to load image {}", m_filename);
    }
    stbi_image_free(data);
}

void Texture::activate(unsigned int slot) {
    // TOOD: multiple textures;
    glActiveTexture(GL_TEXTURE0 + slot);
    bind();
}
