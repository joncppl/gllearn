//
// Created by jonathan on 09/03/19.
//

#include <rendering/Program.hpp>

#include "rendering/Program.hpp"


Program::~Program() {
    if (m_id > 0) {
        trace("Deleting shader program {}.", m_id);
        glDeleteShader(m_id);
    }
}

bool Program::link() {
    trace("Linking shader program.");

    if (!m_id) {
        error("Failed to link shader program since no program has been created.");
        return false;
    }

    for (auto &shader : m_shaders) {
        if (!shader.compile()) {
            return false;
        }

        trace("Attaching shader {} to program {}.", shader.id(), m_id);
        glAttachShader(m_id, shader.id());
    }

    trace("Linking shader program {}.", m_id);
    glLinkProgram(m_id);

    trace("Validating shader program {}.", m_id);
    glValidateProgram(m_id);

    // shaders are no longer necessary
    m_shaders.clear();

    return true;
}

void Program::use() {
    trace("Use program id={}.", m_id);
    glUseProgram(m_id);
}

Program::Program(std::size_t l) {
    m_shaders.reserve(l);
    create_program();
}

Program::Program() {
    m_shaders.reserve(2);
    create_program();
}

Program::Program(std::vector<Shader> &&shaders) : m_shaders(std::move(shaders)) {
    create_program();
}

void Program::create_program() {
    m_id = glCreateProgram();
    if (m_id == 0) {
        error("glCreateProgram failed.");
    } else {
        trace("Created shader program pid={}", m_id);
    }
}
