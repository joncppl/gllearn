//
// Created by jonathan on 16/03/19.
//

#include <rendering/Object.hpp>

#include <utility/EventManager.hpp>

Object::Object(const std::string &name, std::shared_ptr<Material> material) : m_name(name),
                                                                              m_material(std::move(material)) {
    m_gui_rot = glm::eulerAngles(m_rotation);
}

Object::Object(const std::string &name, const glm::vec3 &translation, const glm::quat &rotation, glm::vec3 &scale,
               std::shared_ptr<Material> material) : m_name(name), m_translation(translation),
                                                     m_rotation(rotation), m_scale(scale),
                                                     m_material(std::move(material)) {
    m_gui_rot = glm::eulerAngles(m_rotation);
}

Object::Object(const std::string &name, glm::vec3 &&translation, glm::quat &&rotation, glm::vec4 &&scale,
               std::shared_ptr<Material> material) : m_name(name), m_translation(translation),
                                                     m_rotation(rotation), m_scale(scale),
                                                     m_material(std::move(material)) {
    m_gui_rot = glm::eulerAngles(m_rotation);
}

glm::mat4 Object::translation_matrix() const {
    return glm::translate(glm::mat4(1.0f), m_translation);
}

glm::mat4 Object::rotation_matrix() const {
    return glm::toMat4(m_rotation);
}

glm::mat4 Object::scale_matrix() const {
    return glm::scale(glm::mat4(1.0f), m_scale);
}

glm::mat4 Object::model_matrix() const {
    return translation_matrix() * rotation_matrix() * scale_matrix();
}

void Object::translate(const glm::vec3 &translate) {
    m_translation += translate;
}

void Object::rotate(const glm::vec3 &euler) {
    m_rotation += glm::quat(euler);
}

void Object::rotate(const glm::quat &quat) {
    m_rotation += quat;
    m_gui_rot = glm::eulerAngles(m_rotation);
}

void Object::scale(const glm::vec3 &scale) {
    m_scale *= scale;
}

void Object::set_translation(const glm::vec3 &translation) {
    m_translation = translation;
}

void Object::set_rotation(const glm::quat &quat) {
    m_rotation = quat;
    m_gui_rot = glm::eulerAngles(quat);
}

void Object::set_rotation(const glm::vec3 &euler) {
    m_rotation = glm::quat(euler);
    m_gui_rot = euler;
}

void Object::set_scale(const glm::vec3 &scale) {
    m_scale = scale;
}

void Object::set_material(const std::shared_ptr<Material> &material) {
    m_material = material;
}

std::vector<Vertex> &Object::vertices() {
    m_changed = true;
    return m_vertices;
}

std::vector<unsigned int> &Object::indicies() {
    m_changed = true;
    return m_indicies;
}

std::shared_ptr<Material> &Object::material() {
    m_changed = true;
    return m_material;
}

bool Object::changed() const { return m_changed; }

void Object::clear_changed() {
    m_changed = false;
}

std::function<void()> Object::get_gui_callback() {
    return std::bind(&Object::render_gui, this);
}

void Object::render_gui() {
    set_rotation(m_gui_rot);

    ImGui::Begin(name().c_str());

    ImGui::SliderFloat3("translation", &m_translation.x, -1.0f, 1.0f);
    ImGui::SliderFloat3("scale", &m_scale.x, 0.0f, 1.0f);
    ImGui::SliderFloat3("rotation", &m_gui_rot.x, -2 * M_PIf32, 2 * M_PIf32);

    ImGui::End();
}

void Object::register_event_callbacks(std::shared_ptr<EventManager> em) {
    em->add_key_event_callback(
            std::bind(&Object::key_event_callback, this, std::placeholders::_1, std::placeholders::_2),
            EventManager::ActionFilter({GLFW_KEY_W, GLFW_KEY_A, GLFW_KEY_S, GLFW_KEY_D},
                                       {GLFW_PRESS, GLFW_RELEASE}));
}

void Object::key_event_callback(int code, int action) {
switch (code) {
        case GLFW_KEY_W:
            if (action == GLFW_PRESS)
                m_velocity += glm::vec3{0, 0, 1};
            else if (action == GLFW_RELEASE)
                m_velocity -= glm::vec3{0, 0, 1};
            break;
        case GLFW_KEY_A:
            if (action == GLFW_PRESS)
                m_velocity += glm::vec3{1, 0, 0};
            else if (action == GLFW_RELEASE)
                m_velocity -= glm::vec3{1, 0, 0};
            break;
        case GLFW_KEY_S:
            if (action == GLFW_PRESS)
                m_velocity += glm::vec3{0, 0, -1};
            else if (action == GLFW_RELEASE)
                m_velocity -= glm::vec3{0, 0, -1};
            break;
        case GLFW_KEY_D:
            if (action == GLFW_PRESS)
                m_velocity += glm::vec3{-1, 0, 0};
            else if (action == GLFW_RELEASE)
                m_velocity -= glm::vec3{-1, 0, 0};
            break;
    }
}

void Object::update(float delta_seconds) {
    if (glm::length(m_velocity) > 0) {
        auto normalized_velocity = glm::normalize(m_velocity);
        auto corrected = normalized_velocity * m_speed * delta_seconds;
        translate(corrected);
    }
}

