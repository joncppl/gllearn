//
// Created by jonathan on 09/03/19.
//

#include "context/GLContext.hpp"


static const char *gl_debug_type_str(GLenum type) {
    switch (type) {
        case GL_DEBUG_TYPE_ERROR:
            return "ERROR";
        case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR:
            return "DEPRECATED_BEHAVIOR";
        case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:
            return "UNDEFINED_BEHAVIOR";
        case GL_DEBUG_TYPE_PORTABILITY:
            return "PORTABILITY";
        case GL_DEBUG_TYPE_PERFORMANCE:
            return "PERFORMANCE";
        case GL_DEBUG_TYPE_OTHER:
            return "OTHER";
        case GL_DEBUG_TYPE_MARKER:
            return "MARKER";
        case GL_DEBUG_TYPE_PUSH_GROUP:
            return "PUSH_GROUP";
        case GL_DEBUG_TYPE_POP_GROUP:
            return "POP_GROUP";
        default:
            return "TYPE UNKNOWN";
    }
}

//static const char *gl_debug_severity_str(GLenum severity) {
//    switch (severity) {
//        case GL_DEBUG_SEVERITY_LOW:
//            return "LOW";
//        case GL_DEBUG_SEVERITY_MEDIUM:
//            return "MEDIUM";
//        case GL_DEBUG_SEVERITY_HIGH:
//            return "HIGH";
//        default:
//            return "SEVERITY UNKNOWN";
//    }
//}

static const char *gl_debug_source_str(GLenum source) {
    switch (source) {
        case GL_DEBUG_SOURCE_API:
            return "API";
        case GL_DEBUG_SOURCE_WINDOW_SYSTEM:
            return "WINDOW_SYSTEM";
        case GL_DEBUG_SOURCE_SHADER_COMPILER:
            return "SHADER_COMPILER";
        case GL_DEBUG_SOURCE_THIRD_PARTY:
            return "THIRD_PARTY";
        case GL_DEBUG_SOURCE_APPLICATION:
            return "APPLICATION";
        case GL_DEBUG_SOURCE_OTHER:
        default:
            return "SOURCE UNKNOWN";
    }
}

static void
debug_callback(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar *message,
               const void *user_param
) {
    (void) length;
    (void) (user_param);

    auto logger = get_logger();

#define log(logfun) logger->logfun("GL: [{}] [{}] [{}] {}", gl_debug_source_str(source), gl_debug_type_str(type), id, message)

    switch (severity) {
        case GL_DEBUG_SEVERITY_NOTIFICATION:
            log(info);
            break;
        case GL_DEBUG_SEVERITY_LOW:
            log(info);
            break;
        case GL_DEBUG_SEVERITY_MEDIUM:
            log(warn);
            break;
        case GL_DEBUG_SEVERITY_HIGH:
            log(error);
            break;
        default:
            log(error);
//            logger->error("SEVERITY UNKNOWN");
            break;
    }
}

GLContext::GLContext() {
    trace("Creating GLContext.");

    trace("Initializing GLEW.");
    auto err = glewInit();

    if (err != GLEW_OK) {
        error("glewInit failed.");
    } else {
        m_initialized = true;
        info("GL version: {}", get_gl_version());

        trace("setting gl debug message callback");

#if DEBUG
        glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
        glDebugMessageCallback(&debug_callback, nullptr);
        unsigned int ids = 0;
        glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, &ids, true);
#endif

        glEnable(GL_DEPTH_TEST);
        glEnable(GL_TEXTURE_2D);
        glDepthFunc(GL_LESS);
    }
}

GLContext::~GLContext() {
    trace("Destroying GLContext");
}

std::string GLContext::get_gl_version() {
    return std::string((const char *) glGetString(GL_VERSION));
}
