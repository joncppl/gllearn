//
// Created by jonathan on 16/03/19.
//

#include <context/OverlayGui.hpp>

#include "rendering/Object.hpp"
#include "context/MainWindow.hpp"
#include "rendering/Camera.hpp"

OverlayGui::OverlayGui(const MainWindow &window) {
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGuiIO &io = ImGui::GetIO();
    (void) io;
    //io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;  // Enable Keyboard Controls
    //io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;   // Enable Gamepad Controls

    ImGui::StyleColorsDark();
    //ImGui::StyleColorsClassic();

    // Setup Platform/Renderer bindings
    ImGui_ImplGlfw_InitForOpenGL(window.window(), true);
    ImGui_ImplOpenGL3_Init(imgui_glsl_version);
}

void OverlayGui::add_callback(const std::function<void()> &cb) {
    m_render_callbacks.push_back(cb);
}

void OverlayGui::add_callback(std::function<void()> &&cb) {
    m_render_callbacks.push_back(cb);
}

void OverlayGui::add_object(Object &object) {
    m_render_callbacks.push_back(object.get_gui_callback());
}

void OverlayGui::add_object(const std::shared_ptr<Object> &object) {
    m_render_callbacks.push_back(object->get_gui_callback());
}

void OverlayGui::render() {
    ImGui_ImplOpenGL3_NewFrame();
    ImGui_ImplGlfw_NewFrame();
    ImGui::NewFrame();


    for (const auto &cb : m_render_callbacks) {
        cb();
    }

    ImGui::Render();
    ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
}

void OverlayGui::add_camera(Camera &camera) {
    m_render_callbacks.push_back(camera.get_gui_callback());
}

void OverlayGui::add_camera(const std::shared_ptr<Camera> &camera) {
    m_render_callbacks.push_back(camera->get_gui_callback());
}

void OverlayGui::add_material(Material &material) {
    m_render_callbacks.push_back(material.get_gui_callback());
}

void OverlayGui::add_material(const std::shared_ptr<Material> &material) {
    m_render_callbacks.push_back(material->get_gui_callback());
}

