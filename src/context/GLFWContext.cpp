//
// Created by jonathan on 09/03/19.
//

#include "context/GLFWContext.hpp"

#include <GLFW/glfw3.h>

GLFWContext::GLFWContext() {
    trace("Creating GLFWContext.");

    trace("Setting glfw error callback.");
    glfwSetErrorCallback(&GLFWContext::error_callback);

    trace("Initializing GLFW.");
    auto error = glfwInit();
    if (error == GLFW_FALSE) {
        trace("glfwInit failed.");
        return;
    }

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 4);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GL_TRUE);

    glfwWindowHint(GLFW_SAMPLES, 4);

    glEnable(GL_MULTISAMPLE);

    m_initialized = true;
}

GLFWContext::~GLFWContext() {
    trace("Terminating GLFW.");
    glfwTerminate();
    m_initialized = false;
}

void GLFWContext::error_callback(int errco, const char *errmsg) {
    get_logger()->error("GLFW error [{}]: {}", errco, errmsg);
}

void GLFWContext::poll_events() {
//    log->debug("polling glfw for events.");
    if (m_initialized) {
        glfwPollEvents();
    }
}
