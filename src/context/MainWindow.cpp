//
// Created by jonathan on 09/03/19.
//

#include <context/MainWindow.hpp>

#include <utility/EventManager.hpp>

#include <GLFW/glfw3.h>
#include <context/MainWindow.hpp>

#include <boost/bind.hpp>

std::unordered_map<GLFWwindow *, MainWindow *> g_window_map;

MainWindow::MainWindow(const std::string &title, int width, int height, std::shared_ptr<EventManager> event_manager)
        : m_title(title), m_width(width), m_height(height), m_event_manager(event_manager) {
    trace("Constructing main window {} ({},{})", m_title, m_width, m_height);

    m_window = glfwCreateWindow(m_width, m_height, m_title.c_str(), nullptr, nullptr);

    if (m_window) {
        g_window_map[m_window] = this;

        trace("glfwMakeContextCurrent");
        glfwMakeContextCurrent(m_window);

        // enable v-sync
        trace("Setting swap interval.");
        glfwSwapInterval(1);

        if (!configure_input_callbacks()) {
            destroy();
        }

    } else {
        error("Failed to create glfwWindow");
    }
}

bool MainWindow::configure_input_callbacks() {
    trace("Setting KeyCallback.");
    glfwSetKeyCallback(m_window, &MainWindow::key_callback_dispatch);

    trace("Setting CursorPosCallback.");
    glfwSetInputMode(m_window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
    glfwSetCursorPosCallback(m_window, &MainWindow::cursor_position_callback_dispatch);

    trace("Setting CursorEnterCallback.");
    glfwSetCursorEnterCallback(m_window, &MainWindow::cursor_enter_callback_dispatch);

    trace("Setting MouseButtonCallback.");
    glfwSetMouseButtonCallback(m_window, &MainWindow::mouse_button_callback_dispatch);

    trace("Setting ScrollCallback.");
    glfwSetScrollCallback(m_window, &MainWindow::scroll_callback_dispatch);

    return true;
}

MainWindow::~MainWindow() {
    destroy();
}

void MainWindow::destroy() {
    if (m_window) {
        trace("Destroying main window {}", m_title);
        glfwDestroyWindow(m_window);
        m_window = nullptr;
    }
}

bool MainWindow::window_should_close() {
    if (m_window) {
        return static_cast<bool>(glfwWindowShouldClose(m_window));
    }
    return true;
}

void MainWindow::swap_buffers() {
    if (m_window) {
        glfwSwapBuffers(m_window);
    }
}

void MainWindow::key_callback(int key, int scancode, int action, int mods) {
    m_event_manager->trigger_key_event(key, scancode, action, mods);
}

void MainWindow::cursor_position_callback(double xpos, double ypos) {
    m_event_manager->trigger_cursor_position_event(xpos, ypos);
}

void MainWindow::cursor_enter_callback(int entered) {
    m_event_manager->trigger_cursor_enter_event(entered);
}

void MainWindow::mouse_button_callback(int button, int action, int mods) {
    m_event_manager->trigger_mouse_button_event(button, action, mods);
}

void MainWindow::scroll_callback(double xoffset, double yoffset) {
    m_event_manager->trigger_scroll_event(xoffset, yoffset);
}

#define G_WIN(win, cb) \
    auto win_p = g_window_map.find((win)); \
    if (win_p != g_window_map.end()) { win_p->second->cb; }

void MainWindow::key_callback_dispatch(GLFWwindow *window, int key, int scancode, int action, int mods) {
    G_WIN(window, key_callback(key, scancode, action, mods));
}

void MainWindow::cursor_position_callback_dispatch(GLFWwindow *window, double xpos, double ypos) {
    G_WIN(window, cursor_position_callback(xpos, ypos));
}

void MainWindow::cursor_enter_callback_dispatch(GLFWwindow *window, int entered) {
    G_WIN(window, cursor_enter_callback(entered));
}

void MainWindow::mouse_button_callback_dispatch(GLFWwindow *window, int button, int action, int mods) {
    G_WIN(window, mouse_button_callback(button, action, mods));
}

void MainWindow::scroll_callback_dispatch(GLFWwindow *window, double xoffset, double yoffset) {
    G_WIN(window, scroll_callback(xoffset, yoffset));
}
