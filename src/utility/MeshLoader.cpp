//
// Created by jonathan on 12/03/19.
//

#include "utility/MeshLoader.hpp"

#include <assimp/Importer.hpp>      // C++ importer interface
#include <assimp/scene.h>           // Output data structure
#include <assimp/postprocess.h>


bool MeshLoader::load(std::shared_ptr<Object> &object, const std::string &filename) {
    auto log = get_logger();

    Assimp::Importer importer;

    const aiScene *scene = importer.ReadFile(filename, aiProcess_Triangulate);
    if (!scene) {
        log->error(importer.GetErrorString());
        return false;
    }

    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> normals;
    std::vector<glm::vec2> uvs;
    std::vector<unsigned int> indices;

    // currently support just 1 mesh
    auto mesh = scene->mMeshes[0];

    for (unsigned int i = 0; i < mesh->mNumVertices; i++) {
        auto ai_vertex = mesh->mVertices[i];
        auto ai_normal = mesh->mNormals[i];
        vertices.emplace_back(ai_vertex.x, ai_vertex.y, ai_vertex.z);
        normals.emplace_back(ai_normal.x, ai_normal.y, ai_normal.z);
        if (mesh->mTextureCoords[0]) {
            // assume just 1 texture coord
            auto ai_uv = mesh->mTextureCoords[0][i];
            uvs.emplace_back(ai_uv.x, ai_uv.y);
        }
    }

    for (unsigned int i = 0; i < mesh->mNumFaces; i++) {
        aiFace face = mesh->mFaces[i];
        // retrieve all indices of the face and store them in the indices vector
        for (unsigned int j = 0; j < face.mNumIndices; j++)
            indices.push_back(face.mIndices[j]);
    }

    for (size_t i = 0; i < vertices.size(); i++) {
        object->vertices().emplace_back(vertices[i], normals[i], i < uvs.size() ? uvs[i] : glm::vec3(0, 0, 0));
    }
    object->indicies().assign(indices.begin(), indices.end());

    return true;
}
