#include "utility/log.hpp"

#include "spdlog/sinks/basic_file_sink.h"

#include <vector>
#include <utility/log.hpp>


const char *logger_name = "root";
const char *log_file = "log";

std::vector<spdlog::sink_ptr> sinks;
std::shared_ptr<spdlog::logger> logger;

void initialize_logger() {
#ifdef _WIN32
    sinks.push_back(std::make_shared<spdlog::sinks::wincolor_stdout_sink_mt>());
#else
    sinks.push_back(std::make_shared<spdlog::sinks::ansicolor_stdout_sink_mt>());
#endif
    sinks.push_back(std::make_shared<spdlog::sinks::basic_file_sink_mt>(log_file));
//    sinks.push_back(std::make_shared<vectorsink>());
    logger = std::make_shared<spdlog::logger>(logger_name, begin(sinks), end(sinks));

#if DEBUG
    logger->set_level(spdlog::level::trace);
#else
    logger->set_level(spdlog::level::info);
#endif

    spdlog::register_logger(logger);
}

std::shared_ptr<spdlog::logger> get_logger() {
    return logger;
}

void LogMixin::set_level(spdlog::level::level_enum le) {
    logger->set_level(le);
}
