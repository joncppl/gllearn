#include <pch.hpp>

#include "context/GLFWContext.hpp"
#include "context/MainWindow.hpp"
#include "context/GLContext.hpp"
#include "rendering/Program.hpp"
#include "BaseBuffer.hpp"
#include "rendering/VertexArray.hpp"
#include "rendering/Uniform.hpp"
#include "rendering/Object.hpp"
#include "rendering/Renderer.hpp"
#include "utility/MeshLoader.hpp"
#include "context/OverlayGui.hpp"
#include "rendering/Camera.hpp"
#include "utility/EventManager.hpp"
#include "rendering/Texture.hpp"

#include "shader_defs.h"

int main() {
    initialize_logger();
    auto logger = get_logger();

    auto event_manager = std::make_shared<EventManager>();

    GLFWContext glfw_context;
    MainWindow window("Hello World", 1920, 1080, event_manager);

    if (!window.success()) {
        return 1;
    }

    GLContext gl_context;

    if (!gl_context.initialized()) {
        return 2;
    }

    logger->info("GL version: {}", gl_context.get_gl_version());

    VertexArrayLayout layout;
    layout.add_element<float>(3);
    layout.add_element<float>(3);
    layout.add_element<float>(2);

    Renderer renderer(layout);

    auto object = std::make_shared<Object>("Monkey Head");
    auto object2 = std::make_shared<Object>("Monkey Head 2");
    auto object3 = std::make_shared<Object>("Monkey Head 3");
    auto object4 = std::make_shared<Object>("Monkey Head 4");
    object->register_event_callbacks(event_manager);

    if (!MeshLoader::load(object, "models/suzanne.obj")) {
        return 1;
    }

    object2->indicies() = object->indicies();
    object2->vertices() = object->vertices();
    object3->indicies() = object->indicies();
    object3->vertices() = object->vertices();
    object4->indicies() = object->indicies();
    object4->vertices() = object->vertices();

    auto floor_tex = std::make_shared<Texture>("textures/floor_tile.png");
    auto floor_material = std::make_shared<Material>();
    floor_material->program().add_shader(MAKE_SHADER(GL_VERTEX_SHADER, basic_vertex_shader));
    floor_material->program().add_shader(MAKE_SHADER(GL_FRAGMENT_SHADER, basic_fragment_shader));
    floor_material->set_texture(floor_tex);
    floor_material->ambient_strength(1.0);

    if (!floor_material->program().link()) {
        logger->error("Failed to link shader program.");
        return 3;
    }

    auto dummy_ft = std::make_shared<Object>("dummy_ft");
    if (!MeshLoader::load(dummy_ft, "models/floor_tile.obj")) {
        return 1;
    }

    dummy_ft->set_material(floor_material);

    for (auto i = -10; i <= 10; i += 2) {
        for (auto j = -10; j < 10; j += 2) {
            logger->info("making ft {}, {}", i, j);
            auto ft = std::make_shared<Object>(
                    std::string("floor_tile_") + std::to_string(i) + "_" + std::to_string(j));
            ft->set_translation({i, 0, j});
            ft->set_material(floor_material);
            ft->vertices() = dummy_ft->vertices();
            ft->indicies() = dummy_ft->indicies();
            renderer.add_object(ft);
        }
    }

    auto material = std::make_shared<Material>();
    material->program().add_shader(MAKE_SHADER(GL_VERTEX_SHADER, basic_vertex_shader));
    material->program().add_shader(MAKE_SHADER(GL_FRAGMENT_SHADER, basic_fragment_shader));
    auto tex = std::make_shared<Texture>("textures/suzanne.png");
    material->set_texture(tex);

    if (!material->program().link()) {
        logger->error("Failed to link shader program.");
        return 3;
    }
    material->program().use();
    object->set_translation({-3.0, 1.0, 3.0});
    object2->set_translation({-3.0, 1.0, -3.0});
    object3->set_translation({3.0, 1.0, 3.0});
    object4->set_translation({3.0, 1.0, -3.0});

    object->set_material(material);
    object2->set_material(material);
    object3->set_material(material);
    object4->set_material(material);

    renderer.add_object(object4);
    renderer.add_object(object);
    renderer.add_object(object2);
    renderer.add_object(object3);
//    renderer.add_object(dummy_ft);

    object->scale({0.5f, 0.5f, 0.5f});

    auto camera = std::make_shared<Camera>();
    camera->position() = {0.0f, 8.0f, 10.0f};

    Uniform light_pos_uniform("u_light_pos", material->program());
    glm::vec3 light_pos(0.0f, 0.8f, -0.8f);

    OverlayGui overlay(window);

    overlay.add_callback([]() {
        ImGui::Begin("Debug");
        ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate,
                    ImGui::GetIO().Framerate);
        ImGui::End();
    });

    overlay.add_callback([&]() {
        ImGui::Begin("Main Light");
        ImGui::SliderFloat3("translation", &light_pos.x, -1.0f, 1.0f);
        ImGui::End();
    });

    overlay.add_object(object4);
    overlay.add_object(object);
    overlay.add_object(object2);
    overlay.add_object(object3);
    overlay.add_camera(camera);
    overlay.add_material(material);

    bool render_overlay = false;

    event_manager->add_key_event_callback([&render_overlay](int code, int action) {
        render_overlay = !render_overlay;
    }, EventManager::ActionFilter({GLFW_KEY_F8}, {GLFW_RELEASE}));

    auto t_start = std::chrono::steady_clock::now();

    /* Loop until the user closes the window */
    while (!window.window_should_close()) {
        /* Render here */
        glClearColor(0.5294117647058824f, 0.807843137254902f, 0.9215686274509803f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        int display_w, display_h;
        glfwMakeContextCurrent(window.window());
        glfwGetFramebufferSize(window.window(), &display_w, &display_h);
        glViewport(0, 0, display_w, display_h);

        camera->display_w(display_w);
        camera->display_h(display_h);

        light_pos_uniform.set(light_pos);

        auto t_now = std::chrono::steady_clock::now();
        auto seconds = std::chrono::duration<float>(t_now - t_start).count();
        logger->info("{}s elapsed", seconds);
        object->update(seconds);

        t_start = t_now;

        renderer.render(camera);

        /* Poll for and process events */
        glfw_context.poll_events();

        if (render_overlay) {
            overlay.render();
        }

        /* Swap front and back buffers */
        window.swap_buffers();
    }


    return 0;
}
